package practice.blog.domain.common;

public abstract class DomainException extends RuntimeException {
    protected DomainException(String msg) {
        super(msg);
    }
}
