package practice.blog.domain.entities.valueObjects.builder;


import practice.blog.domain.entities.validator.Validator;

public class Director {
    public void createTitle(Builder builder, String value) {
        builder.setValue(value);
    }
    
    public void createTitleWithValidation(Builder builder, String value, Validator... validators) {
        builder.setValue(value);
        builder.setValidator(validators);
    }
    
}

