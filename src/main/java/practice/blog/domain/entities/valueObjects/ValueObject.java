package practice.blog.domain.entities.valueObjects;

public interface ValueObject<T> {
    T getValue();
}
