package practice.blog.domain.entities.valueObjects;

import practice.blog.domain.entities.validator.Validator;

import java.util.ArrayList;

public final class TextValueObject extends BaseValueObject<String> {
    
    private TextValueObject(String value) throws ValueObjectException {
        super(value);
    }
    
    public static TextValueObject create(String value) throws ValueObjectException {
        return new TextValueObject(value);
    }
    
    public static TextValueObject create(String value, Validator<String> validator) throws ValueObjectException {
        final boolean isValid = validator.check(value);
        
        if (!isValid) {
            final ArrayList<String> errors = validator.getErrors();
            throw new ValueObjectException(errors.toString());
        }
        
        return new TextValueObject(value);
    }
}
