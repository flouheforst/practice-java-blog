package practice.blog.domain.entities.valueObjects;

public class ValueObjectException extends Exception {
    public ValueObjectException(String m) {
        super(m);
    }
}
