package practice.blog.domain.entities.valueObjects;

import practice.blog.domain.entities.validator.Validator;

import java.util.ArrayList;

public final class EmailValueObject extends BaseValueObject<String> {
    private EmailValueObject(String value) {
        super(value);
    }
    
    public static EmailValueObject create(String value) throws ValueObjectException {
        return new EmailValueObject(value);
    }
    
    public static EmailValueObject create(String value, Validator<String> validator) throws ValueObjectException {
        final boolean isValid = validator.check(value);
        
        if (!isValid) {
            final ArrayList<String> errors = validator.getErrors();
            throw new ValueObjectException(errors.toString());
        }
        
        return new EmailValueObject(value);
    }
}

