package practice.blog.domain.entities.valueObjects.builder;

import practice.blog.domain.entities.validator.Validator;
import practice.blog.domain.entities.valueObjects.TextValueObject;
import practice.blog.domain.entities.valueObjects.ValueObject;
import practice.blog.domain.entities.valueObjects.ValueObjectException;

import java.util.ArrayList;
import java.util.Collections;

public class TextValueObjectBuilder implements Builder<ValueObject<String>, String> {
    private String value;
    private ArrayList<Validator> validators = new ArrayList<>();
    
    public void setValue(String value) {
        this.value = value;
    }
    
    @Override
    public void setValidator(Validator... validators) {
        Collections.addAll(this.validators, validators);
    }
    
    
    public TextValueObject create() throws ValueObjectException {
        Validator tittleValidator = Validator.build(validators);
        return TextValueObject.create(value, tittleValidator);
    }
}

