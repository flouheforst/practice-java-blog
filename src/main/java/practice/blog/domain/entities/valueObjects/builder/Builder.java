package practice.blog.domain.entities.valueObjects.builder;


import practice.blog.domain.entities.validator.Validator;
import practice.blog.domain.entities.valueObjects.ValueObjectException;

public interface Builder<T, V> {
    T create() throws ValueObjectException;
    
    void setValue(V value);
    
    void setValidator(Validator... validators);
    
}

