package practice.blog.domain.entities.valueObjects;

import practice.blog.domain.entities.validator.Validator;

import java.util.ArrayList;

public final class ImageS3ValueObject extends BaseValueObject<String> {
    
    private ImageS3ValueObject(String value) throws ValueObjectException {
        super(value);
    }
    
    
    public static ImageS3ValueObject create(String value) throws ValueObjectException {
        return new ImageS3ValueObject(value);
    }
    
    public static ImageS3ValueObject create(String value, Validator<String> validator) throws ValueObjectException {
        final boolean isValid = validator.check(value);
        
        if (!isValid) {
            final ArrayList<String> errors = validator.getErrors();
            throw new ValueObjectException(errors.toString());
        }
        
        return new ImageS3ValueObject(value);
    }
}
