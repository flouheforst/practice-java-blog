package practice.blog.domain.entities.valueObjects;


import java.util.Objects;

public abstract class BaseValueObject<T> implements ValueObject<T> {
    private T value;
    
    public BaseValueObject(T value) {
        this.value = value;
    }
    
    public T getValue() {
        return this.value;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        BaseValueObject<T> that = (BaseValueObject<T>) o;
        return Objects.equals(value, that.value);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
    
    @Override
    public String toString() {
        return this.value.toString();
    }
}



