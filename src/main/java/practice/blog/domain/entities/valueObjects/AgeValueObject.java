package practice.blog.domain.entities.valueObjects;

import practice.blog.domain.entities.validator.Validator;

import java.util.ArrayList;

public final class AgeValueObject extends BaseValueObject<Integer> {
    
    private AgeValueObject(Integer value) {
        super(value);
    }
    
    public static AgeValueObject create(Integer value) {
        return new AgeValueObject(value);
    }
    
    public static AgeValueObject create(Integer value, Validator<Integer> validator) throws ValueObjectException {
        final boolean isValid = validator.check(value);
        
        if (!isValid) {
            final ArrayList<String> errors = validator.getErrors();
            throw new ValueObjectException(errors.toString());
        }
        
        return new AgeValueObject(value);
    }
}
