package practice.blog.domain.entities;

import practice.blog.domain.entities.valueObjects.TextValueObject;

public class CategoryEntity extends BaseEntity {
    
    private final TextValueObject name;
    
    private CategoryEntity(TextValueObject name) {
        this.name = name;
    }
    
    public TextValueObject getName() {
        return this.name;
    }
    
    public static CategoryEntity create(TextValueObject value) {
        return new CategoryEntity(value);
    }
}

