package practice.blog.domain.entities;


import practice.blog.domain.entities.common.UserRole;
import practice.blog.domain.entities.valueObjects.AgeValueObject;
import practice.blog.domain.entities.valueObjects.EmailValueObject;
import practice.blog.domain.entities.valueObjects.ImageS3ValueObject;
import practice.blog.domain.entities.valueObjects.ValueObjectException;

public class UserEntity extends BaseEntity {
    private final EmailValueObject email;
    
    private final ImageS3ValueObject image;
    
    private AgeValueObject age;
    
    private UserRole role;
    
    private UserEntity(EmailValueObject email, ImageS3ValueObject image) {
        super();
        this.email = email;
        this.image = image;
    }
    
    private UserEntity(String id, EmailValueObject email, ImageS3ValueObject image) {
        super(id);
        this.email = email;
        this.image = image;
    }
    
    private UserEntity(EmailValueObject email, ImageS3ValueObject image, AgeValueObject age) {
        super();
        this.email = email;
        this.image = image;
        this.age = age;
    }
    
    private UserEntity(String id, EmailValueObject email, ImageS3ValueObject image, AgeValueObject age) {
        super(id);
        this.email = email;
        this.image = image;
        this.age = age;
    }
    
    private UserEntity(String id, EmailValueObject email, ImageS3ValueObject image, AgeValueObject age, UserRole role) {
        super(id);
        this.email = email;
        this.image = image;
        this.age = age;
        this.role = role;
    }
    
    private UserEntity(EmailValueObject email, ImageS3ValueObject image, AgeValueObject age, UserRole role) {
        this.email = email;
        this.image = image;
        this.age = age;
        this.role = role;
    }
    
    
    public static UserEntity create(String email, String image) throws ValueObjectException {
        final EmailValueObject emailValueObject = EmailValueObject.create(email);
        final ImageS3ValueObject imageS3ValueObject = ImageS3ValueObject.create(image);
        
        return new UserEntity(
                emailValueObject,
                imageS3ValueObject
        );
    }
    
    public static UserEntity create(String id, String email, String image) throws ValueObjectException {
        final EmailValueObject emailValueObject = EmailValueObject.create(email);
        final ImageS3ValueObject imageS3ValueObject = ImageS3ValueObject.create(image);
        
        return new UserEntity(
                id,
                emailValueObject,
                imageS3ValueObject
        );
    }
    
    public static UserEntity create(String email, String image, int age) throws ValueObjectException {
        final EmailValueObject emailValueObject = EmailValueObject.create(email);
        final ImageS3ValueObject imageS3ValueObject = ImageS3ValueObject.create(image);
        final AgeValueObject ageValueObject = AgeValueObject.create(age);
        
        return new UserEntity(
                emailValueObject,
                imageS3ValueObject,
                ageValueObject
        );
    }
    
    public static UserEntity create(String id, String email, String image, int age) throws ValueObjectException {
        final EmailValueObject emailValueObject = EmailValueObject.create(email);
        final ImageS3ValueObject imageS3ValueObject = ImageS3ValueObject.create(image);
        final AgeValueObject ageValueObject = AgeValueObject.create(age);
        
        return new UserEntity(
                id,
                emailValueObject,
                imageS3ValueObject,
                ageValueObject
        );
    }
    
    
    public static UserEntity create(String id, String email, String image, int age, UserRole role) throws ValueObjectException {
        final EmailValueObject emailValueObject = EmailValueObject.create(email);
        final ImageS3ValueObject imageS3ValueObject = ImageS3ValueObject.create(image);
        final AgeValueObject ageValueObject = AgeValueObject.create(age);
        
        return new UserEntity(
                id,
                emailValueObject,
                imageS3ValueObject,
                ageValueObject,
                role
        );
    }
    
    public static UserEntity create(String email, String image, int age, UserRole role) throws ValueObjectException {
        final EmailValueObject emailValueObject = EmailValueObject.create(email);
        final ImageS3ValueObject imageS3ValueObject = ImageS3ValueObject.create(image);
        final AgeValueObject ageValueObject = AgeValueObject.create(age);
        
        return new UserEntity(
                emailValueObject,
                imageS3ValueObject,
                ageValueObject,
                role
        );
    }
    
    public Integer getAge() {
        return age.getValue();
    }
    
    public UserRole getRole() {
        return role;
    }
    
    public String getEmail() {
        return this.email.getValue();
    }
    
    public String getImage() {
        return this.image.getValue();
    }
    
}

