package practice.blog.domain.entities.common;

public enum PostStatus {
    NEW,
    APPROVED,
    CANCELED,
}
