package practice.blog.domain.entities.common;

import practice.blog.utils.GenerateUuid;

public class Identity {
    private String value;
    
    public Identity() {
        this.value = GenerateUuid.UUID();
    }
    
    public Identity(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return this.value;
    }
}
