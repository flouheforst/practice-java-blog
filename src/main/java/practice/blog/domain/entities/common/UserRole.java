package practice.blog.domain.entities.common;

public enum UserRole {
    CUSTOMER,
    ADMIN
}
