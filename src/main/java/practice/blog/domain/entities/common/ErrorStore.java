package practice.blog.domain.entities.common;

import java.util.ArrayList;

public class ErrorStore {
    private ArrayList<String> errors = new ArrayList();
    
    public void add(String msg) {
        errors.add(msg);
    }
    
    public ArrayList<String> get() {
        return errors;
    }
}
