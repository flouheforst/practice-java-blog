package practice.blog.domain.entities.validator;

public class Range extends Validator<Integer> {
    private Integer min;
    private Integer max;
    
    private String msgError;
    
    public Range(Integer min, Integer end) {
        this.min = min;
        this.max = end;
        this.msgError = "Range is wrong, min: " + min + ", max: " + end;
    }
    
    @Override
    public boolean check(Integer value) {
        if (value < min || value > max) {
            this.store.add(this.msgError);
        }
        
        return checkNext(value);
    }
}
