package practice.blog.domain.entities.validator;

import practice.blog.domain.entities.common.ErrorStore;

import java.util.ArrayList;

public abstract class Validator<T> {
    
    private Validator next;
    protected ErrorStore store;
    
    public abstract boolean check(T value);
    
    public static Validator build(ArrayList<Validator> validators) {
        ErrorStore errorStore = new ErrorStore();
        
        for (Validator validator : validators) {
            validator.setStore(errorStore);
        }
        
        
        Validator head = validators.get(0);
        
        for (int i = 1; i < validators.size(); i++) {
            head.next = validators.get(i);
            head = validators.get(i);
        }
        
        
        return validators.get(0);
    }
    
    public void setStore(ErrorStore store) {
        this.store = store;
    }
    
    public ArrayList<String> getErrors() {
        return this.store.get();
    }
    
    protected boolean checkNext(T value) {
        if (next == null) {
            if (this.store.get().size() > 0) {
                return false;
            }
            return true;
        }
        return next.check(value);
    }
}
