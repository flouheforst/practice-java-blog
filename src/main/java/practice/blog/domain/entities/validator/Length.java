package practice.blog.domain.entities.validator;

public class Length extends Validator<String> {
    private int maxLength;
    private int minLength;
    private String msgError;
    
    public Length(Integer minLength, Integer maxLength) {
        this.maxLength = maxLength;
        this.minLength = minLength;
        this.msgError = "Length is wrong, min: " + minLength + ", max: " + maxLength;
    }
    
    @Override
    public boolean check(String value) {
        final int length = value.length();
        
        if (length < minLength || length > maxLength) {
            this.store.add(this.msgError);
        }
        
        return checkNext(value);
    }
}
