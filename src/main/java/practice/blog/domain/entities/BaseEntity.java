package practice.blog.domain.entities;

import practice.blog.domain.entities.common.Identity;

import java.lang.reflect.Field;

public class BaseEntity {
    private Identity id;
    
    public BaseEntity() {
        final Identity identity = new Identity();
        this.id = identity;
    }
    
    public BaseEntity(String id) {
        final Identity identity = new Identity(id);
        this.id = identity;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final BaseEntity castedToBase = (BaseEntity) obj;
        return this.id.getValue() == castedToBase.id.getValue();
    }
    
    public String getId() {
        return this.id.getValue();
    }
    
    @Override
    public String toString() {
        final String className = this.getClass().getName();
        Field[] fields = this.getClass().getDeclaredFields();
        StringBuilder toPrint = new StringBuilder();
        
        
        toPrint.append(className);
        toPrint.append(": {");
        
        toPrint.append("\n");
        
        toPrint.append("\t");
        toPrint.append("id");
        toPrint.append(": ");
        toPrint.append(this.id.getValue() + ",");
        toPrint.append("\n");
        
        for (Field field : fields) {
            final String fieldName = field.getName();
            field.setAccessible(true);
            
            toPrint.append("\t");
            toPrint.append(fieldName);
            toPrint.append(": ");
            try {
                Object fieldValue = field.get(this);
                if (fieldValue != null) {
                    String value = fieldValue.toString();
                    toPrint.append(value + ",");
                } else {
                    toPrint.append("null" + ",");
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
            toPrint.append("\n");
        }
        toPrint.append("}");
        
        return toPrint.toString();
    }
}
