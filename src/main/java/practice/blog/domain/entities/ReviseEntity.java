package practice.blog.domain.entities;


import practice.blog.domain.entities.validator.Length;
import practice.blog.domain.entities.valueObjects.TextValueObject;
import practice.blog.domain.entities.valueObjects.ValueObjectException;
import practice.blog.domain.entities.valueObjects.builder.Director;
import practice.blog.domain.entities.valueObjects.builder.TextValueObjectBuilder;

public class ReviseEntity extends BaseEntity {
    private static final int MAX_TEXT_LENGTH = 500;
    private static final int MIN_TEXT_LENGTH = 3;
    private static final Director director = new Director();
    private PostEntity post;
    private UserEntity adminUser;
    private TextValueObject reason;
    
    private ReviseEntity(PostEntity post, UserEntity adminUser, TextValueObject reason) {
        super();
        this.post = post;
        this.adminUser = adminUser;
        this.reason = reason;
    }
    
    private static TextValueObject createReason(String text) throws ValueObjectException {
        TextValueObjectBuilder textValueObjectBuilder = new TextValueObjectBuilder();
        ReviseEntity.director.createTitleWithValidation(
                textValueObjectBuilder,
                text,
                new Length(ReviseEntity.MIN_TEXT_LENGTH, ReviseEntity.MAX_TEXT_LENGTH)
        );
        
        return textValueObjectBuilder.create();
    }
    
    public static ReviseEntity create(PostEntity post, UserEntity adminUser, String reason) throws ValueObjectException {
        final TextValueObject reasonValueObject = ReviseEntity.createReason(reason);
        return new ReviseEntity(post, adminUser, reasonValueObject);
    }
    
    public String getReason() {
        return reason.getValue();
    }
}

