package practice.blog.domain.entities;


import practice.blog.domain.entities.common.PostStatus;
import practice.blog.domain.entities.validator.Length;
import practice.blog.domain.entities.valueObjects.ImageS3ValueObject;
import practice.blog.domain.entities.valueObjects.TextValueObject;
import practice.blog.domain.entities.valueObjects.ValueObjectException;
import practice.blog.domain.entities.valueObjects.builder.Director;
import practice.blog.domain.entities.valueObjects.builder.TextValueObjectBuilder;

public class PostEntity extends BaseEntity {
    private static final Director director = new Director();
    private static final int MIN_TITLE_LENGTH = 2;
    private static final int MAX_TITLE_LENGTH = 120;
    private static final int MIN_TEXT_LENGTH = 1;
    private static final int MAX_TEXT_LENGTH = 1024;
    private TextValueObject title;
    private TextValueObject text;
    private ImageS3ValueObject image;
    private PostStatus status;
    private UserEntity user;
    private ReviseEntity revise;
    
    private PostEntity(TextValueObject title, TextValueObject text, ImageS3ValueObject image, PostStatus status) {
        super();
        this.title = title;
        this.text = text;
        this.image = image;
        this.status = status;
    }
    
    private PostEntity(TextValueObject title, TextValueObject text, ImageS3ValueObject image, PostStatus status, UserEntity user) {
        super();
        this.title = title;
        this.text = text;
        this.image = image;
        this.status = status;
        this.user = user;
        
    }
    
    private PostEntity(String id, TextValueObject title, TextValueObject text, ImageS3ValueObject image, PostStatus status) {
        super(id);
        this.title = title;
        this.text = text;
        this.image = image;
        this.status = status;
    }
    
    private PostEntity(String id, TextValueObject title, TextValueObject text, ImageS3ValueObject image, PostStatus status, UserEntity user) {
        super(id);
        this.title = title;
        this.text = text;
        this.image = image;
        this.status = status;
        this.user = user;
    }
    
    private static TextValueObject createTitle(String title) throws ValueObjectException {
        TextValueObjectBuilder titleValueObjectBuilder = new TextValueObjectBuilder();
        PostEntity.director.createTitleWithValidation(
                titleValueObjectBuilder,
                title,
                new Length(PostEntity.MIN_TITLE_LENGTH, PostEntity.MAX_TITLE_LENGTH)
        );
        
        return titleValueObjectBuilder.create();
    }
    
    private static TextValueObject createText(String text) throws ValueObjectException {
        TextValueObjectBuilder textValueObjectBuilder = new TextValueObjectBuilder();
        PostEntity.director.createTitleWithValidation(
                textValueObjectBuilder,
                text,
                new Length(PostEntity.MIN_TEXT_LENGTH, PostEntity.MAX_TEXT_LENGTH)
        );
        
        return textValueObjectBuilder.create();
    }
    
    public static PostEntity create(String id, String title, String text, String image) throws ValueObjectException {
        final TextValueObject titleValueObject = PostEntity.createTitle(title);
        final TextValueObject textValueObject = PostEntity.createText(text);
        
        
        final ImageS3ValueObject imageS3ValueObject = ImageS3ValueObject.create(image);
        return new PostEntity(id, titleValueObject, textValueObject, imageS3ValueObject, PostStatus.NEW);
    }
    
    public static PostEntity create(String id, String title, String text, String image, UserEntity user) throws ValueObjectException {
        final TextValueObject titleValueObject = PostEntity.createTitle(title);
        final TextValueObject textValueObject = PostEntity.createText(text);
        
        final ImageS3ValueObject imageS3ValueObject = ImageS3ValueObject.create(image);
        
        return new PostEntity(id, titleValueObject, textValueObject, imageS3ValueObject, PostStatus.NEW, user);
    }
    
    public static PostEntity create(String title, String text, String image) throws ValueObjectException {
        final TextValueObject titleValueObject = PostEntity.createTitle(title);
        final TextValueObject textValueObject = PostEntity.createText(text);
        
        final ImageS3ValueObject imageS3ValueObject = ImageS3ValueObject.create(image);
        
        return new PostEntity(titleValueObject, textValueObject, imageS3ValueObject, PostStatus.NEW);
    }
    
    public static PostEntity create(String title, String text, String image, UserEntity user) throws ValueObjectException {
        final TextValueObject titleValueObject = PostEntity.createTitle(title);
        final TextValueObject textValueObject = PostEntity.createText(text);
        
        final ImageS3ValueObject imageS3ValueObject = ImageS3ValueObject.create(image);
        
        return new PostEntity(titleValueObject, textValueObject, imageS3ValueObject, PostStatus.NEW, user);
    }
    
    public String getTitle() {
        return this.title.getValue();
    }
    
    public String getText() {
        return this.text.getValue();
    }
    
    public String getImage() {
        return this.image.getValue();
    }
    
    public PostStatus getStatus() {
        return this.status;
    }
    
    public void approve() {
        this.status = PostStatus.APPROVED;
    }
    
    public void cancel(ReviseEntity revise) {
        this.status = PostStatus.CANCELED;
        this.revise = revise;
    }
}

