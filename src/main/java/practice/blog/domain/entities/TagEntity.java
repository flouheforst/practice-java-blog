package practice.blog.domain.entities;

import practice.blog.domain.entities.valueObjects.TextValueObject;

public class TagEntity extends BaseEntity {
    private final TextValueObject name;
    
    
    private TagEntity(TextValueObject name) {
        this.name = name;
    }
    
    public TextValueObject getName() {
        return this.name;
    }
    
    public static TagEntity create(TextValueObject value) {
        return new TagEntity(value);
    }
}
