package practice.blog.domain.useCases.user.create.dto;

import practice.blog.domain.entities.common.UserRole;

public final class UserCreateRequestDto {
    
    private final String email;
    private final String image;
    private final Integer age;
    private final UserRole role;
    
    public UserCreateRequestDto(String email, String image, Integer age, UserRole role) {
        this.email = email;
        this.image = image;
        this.age = age;
        this.role = role;
    }
    
    public String getEmail() {
        return email;
    }
    
    public String getImage() {
        return image;
    }
    
    public Integer getAge() {
        return age;
    }
    
    public UserRole getRole() {
        return role;
    }
}
