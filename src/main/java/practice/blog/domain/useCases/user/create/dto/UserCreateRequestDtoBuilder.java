package practice.blog.domain.useCases.user.create.dto;


import practice.blog.domain.entities.common.UserRole;

public class UserCreateRequestDtoBuilder {
    
    private String email;
    private String image;
    private Integer age;
    private UserRole role;
    
    public UserCreateRequestDtoBuilder setEmail(String email) {
        this.email = email;
        return this;
    }
    
    public UserCreateRequestDtoBuilder setImage(String image) {
        this.image = image;
        return this;
    }
    
    public UserCreateRequestDtoBuilder setAge(Integer age) {
        this.age = age;
        return this;
    }
    
    public UserCreateRequestDtoBuilder setRole(UserRole role) {
        this.role = role;
        return this;
    }
    
    public UserCreateResponseDto build() {
        return new UserCreateResponseDto(this.email, this.image, this.age, this.role);
    }
}
