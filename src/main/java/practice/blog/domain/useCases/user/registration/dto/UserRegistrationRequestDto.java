package practice.blog.domain.useCases.user.registration.dto;

public class UserRegistrationRequestDto {
    
    private final String email;
    private final String image;
    private final Integer age;
    
    public UserRegistrationRequestDto(String email, String image, Integer age) {
        this.email = email;
        this.image = image;
        this.age = age;
    }
    
    public String getEmail() {
        return email;
    }
    
    public String getImage() {
        return image;
    }
    
    public Integer getAge() {
        return age;
    }
    
    
}
