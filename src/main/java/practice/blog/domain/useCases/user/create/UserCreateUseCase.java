package practice.blog.domain.useCases.user.create;

import practice.blog.domain.entities.UserEntity;
import practice.blog.domain.entities.valueObjects.ValueObjectException;
import practice.blog.domain.persistence.CreateUser;
import practice.blog.domain.services.UserService;
import practice.blog.domain.useCases.user.create.dto.UserCreateRequestDto;
import practice.blog.domain.useCases.user.create.dto.UserCreateResponseDto;
import practice.blog.domain.useCases.user.create.errors.UserCreateAlreadyExist;

import java.util.Optional;

public class UserCreateUseCase {
    
    private final UserService userService;
    private final CreateUser createUser;
    
    public UserCreateUseCase(
            UserService userService,
            CreateUser createUser
    ) {
        this.userService = userService;
        this.createUser = createUser;
    }
    
    public UserCreateResponseDto execute(UserCreateRequestDto userDataCreate) throws ValueObjectException, UserCreateAlreadyExist {
        Optional<UserEntity> user = this.userService.getByField("email", userDataCreate.getEmail());
        
        if (user.isPresent()) {
            throw new UserCreateAlreadyExist();
        }
        
        UserEntity newUser = UserEntity.create(
                userDataCreate.getEmail(),
                userDataCreate.getImage(),
                userDataCreate.getAge(),
                userDataCreate.getRole()
        );
        
        this.createUser.create(newUser);
        
        return UserCreateResponseDto.builder()
                .setRole(newUser.getRole())
                .setAge(newUser.getAge())
                .setEmail(newUser.getEmail())
                .setImage(newUser.getImage())
                .build();
    }
}
