package practice.blog.domain.useCases.user.create.errors;

import practice.blog.domain.common.DomainException;

public class UserCreateAlreadyExist extends DomainException {
    public static final String MESSAGE = "User already exist";
    
    public UserCreateAlreadyExist() {
        super(UserCreateAlreadyExist.MESSAGE);
    }
}
