package practice.blog.domain.useCases.user.registration.dto;

import practice.blog.domain.entities.common.UserRole;
import practice.blog.domain.useCases.user.create.dto.UserCreateRequestDtoBuilder;

public class UserRegistrationResponseDto {
    private final String email;
    private final String image;
    private final Integer age;
    private final UserRole role;
    
    
    public UserRegistrationResponseDto(String email, String image, Integer age, UserRole role) {
        this.email = email;
        this.image = image;
        this.age = age;
        this.role = role;
    }
    
    public static UserCreateRequestDtoBuilder builder() {
        return new UserCreateRequestDtoBuilder();
    }
    
    public String getEmail() {
        return email;
    }
    
    public String getImage() {
        return image;
    }
    
    public Integer getAge() {
        return age;
    }
    
    public UserRole getRole() {
        return role;
    }
}
