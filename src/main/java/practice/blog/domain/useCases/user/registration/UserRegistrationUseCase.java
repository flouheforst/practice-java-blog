package practice.blog.domain.useCases.user.registration;

import practice.blog.adapters.mail.MailAdapter;
import practice.blog.adapters.mail.dto.MailRequestDto;
import practice.blog.domain.entities.common.UserRole;
import practice.blog.domain.entities.valueObjects.ValueObjectException;
import practice.blog.domain.useCases.user.create.UserCreateUseCase;
import practice.blog.domain.useCases.user.create.dto.UserCreateRequestDto;
import practice.blog.domain.useCases.user.create.dto.UserCreateResponseDto;
import practice.blog.domain.useCases.user.registration.dto.UserRegistrationRequestDto;
import practice.blog.domain.useCases.user.registration.dto.UserRegistrationResponseDto;
import practice.blog.utils.GenerateRandomNumber;

public class UserRegistrationUseCase {
    private static final Integer COUNT_NUMBERS = 4;
    private final UserCreateUseCase userCreateUseCase;
    private final MailAdapter mailAdapter;
    
    public UserRegistrationUseCase(
            UserCreateUseCase userCreateUseCase,
            MailAdapter mailAdapter
    ) {
        this.userCreateUseCase = userCreateUseCase;
        this.mailAdapter = mailAdapter;
    }
    
    public UserRegistrationResponseDto execute(UserRegistrationRequestDto userRegistrationRequestDto) throws ValueObjectException {
        UserCreateRequestDto userCreateRequestDto = new UserCreateRequestDto(
                userRegistrationRequestDto.getEmail(),
                userRegistrationRequestDto.getImage(),
                userRegistrationRequestDto.getAge(),
                UserRole.CUSTOMER
        );
        
        UserCreateResponseDto userEntity = this.userCreateUseCase.execute(userCreateRequestDto);
        final String code = GenerateRandomNumber.randomNumber(UserRegistrationUseCase.COUNT_NUMBERS);
        
        MailRequestDto mailRequestDto = MailRequestDto.builder()
                .setEmail(userEntity.getEmail())
                .setCode(code)
                .build();
        
        this.mailAdapter.send(mailRequestDto);
        
        return new UserRegistrationResponseDto(
                userEntity.getEmail(),
                userEntity.getImage(),
                userEntity.getAge(),
                userEntity.getRole()
        );
    }
}
