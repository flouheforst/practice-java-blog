package practice.blog.domain.useCases.post.revise;

import practice.blog.domain.entities.PostEntity;
import practice.blog.domain.entities.ReviseEntity;
import practice.blog.domain.entities.UserEntity;
import practice.blog.domain.entities.common.PostStatus;
import practice.blog.domain.entities.valueObjects.ValueObjectException;
import practice.blog.domain.persistence.UpdatePost;
import practice.blog.domain.services.PostService;
import practice.blog.domain.services.UserService;
import practice.blog.domain.useCases.post.revise.dto.RevisePostRequestDto;
import practice.blog.domain.useCases.post.revise.dto.RevisePostResponseDto;
import practice.blog.domain.useCases.post.revise.errors.RevisePostIsNotNew;
import practice.blog.domain.useCases.post.revise.errors.RevisePostReasonMustBe;

import java.util.Optional;

public class RevisePostUseCase {
    UserService userService;
    PostService postService;
    UpdatePost updatePost;
    
    RevisePostUseCase(
            UserService userService,
            PostService postService,
            UpdatePost updatePost
    ) {
        this.userService = userService;
        this.postService = postService;
        this.updatePost = updatePost;
    }
    
    public RevisePostResponseDto execute(RevisePostRequestDto data) throws ValueObjectException, RevisePostIsNotNew, RevisePostReasonMustBe {
        UserEntity adminUser = this.userService.getByFieldOrFail("id", data.getAdminId());
        PostEntity post = this.postService.getByFieldOrFail("id", data.getPostId());
        
        if (post.getStatus() != PostStatus.NEW) {
            throw new RevisePostIsNotNew();
        }
        
        if (!data.getIsBlock()) {
            post.approve();
            
            return new RevisePostResponseDto(
                    post.getId(),
                    adminUser.getId(),
                    data.getIsBlock()
            );
        }
        
        final Optional<String> reason = data.getReason();
        
        if (reason.isEmpty()) {
            throw new RevisePostReasonMustBe();
        }
        
        final String valueReason = reason.get();
        final ReviseEntity revise = ReviseEntity.create(post, adminUser, valueReason);
        
        post.cancel(revise);
        
        this.updatePost.update(post);
        
        return new RevisePostResponseDto(
                post.getId(),
                adminUser.getId(),
                data.getIsBlock(),
                revise.getReason()
        );
    }
}
