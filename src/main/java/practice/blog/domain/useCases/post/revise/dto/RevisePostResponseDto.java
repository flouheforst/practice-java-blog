package practice.blog.domain.useCases.post.revise.dto;


import java.util.Optional;

public class RevisePostResponseDto {
    private String postId;
    
    private String adminId;
    
    private boolean isBlock;
    
    private Optional<String> reason;
    
    public RevisePostResponseDto(String postId, String adminId, boolean isBlock) {
        this.postId = postId;
        this.adminId = adminId;
        this.isBlock = isBlock;
    }
    
    public RevisePostResponseDto(String postId, String adminId, boolean isBlock, String reason) {
        this.postId = postId;
        this.adminId = adminId;
        this.isBlock = isBlock;
        this.reason = Optional.of(reason);
    }
    
    public boolean getIsBlock() {
        return isBlock;
    }
    
    public String getPostId() {
        return postId;
    }
    
    public String getAdminId() {
        return adminId;
    }
    
    public Optional<String> getReason() {
        return reason;
    }
}

