package practice.blog.domain.useCases.post.create;

import practice.blog.domain.entities.PostEntity;
import practice.blog.domain.entities.valueObjects.ValueObjectException;
import practice.blog.domain.persistence.CreatePost;
import practice.blog.domain.services.PostService;
import practice.blog.domain.services.UserService;
import practice.blog.domain.useCases.post.create.dto.PostCreateRequestDto;
import practice.blog.domain.useCases.post.create.dto.PostCreateResponseDto;
import practice.blog.domain.useCases.post.create.errors.PostCreateAlreadyExist;

import java.util.Optional;

public class PostCreateUseCase {
    private final PostService postService;
    private final UserService userService;
    private final CreatePost createPost;
    
    public PostCreateUseCase(UserService userService, PostService postService, CreatePost createPost) {
        this.userService = userService;
        this.postService = postService;
        this.createPost = createPost;
    }
    
    public PostCreateResponseDto execute(PostCreateRequestDto postData) throws ValueObjectException, PostCreateAlreadyExist {
        final String userId = postData.getUserId();
        final String title = postData.getTitle();
        final String image = postData.getImage();
        final String text = postData.getText();
        
        this.userService.getByFieldOrFail("id", userId);
        final Optional<PostEntity> post = this.postService.getByField("title", title);
        
        if (post.isPresent()) {
            throw new PostCreateAlreadyExist();
        }
        
        final PostEntity newPost = PostEntity.create(title, text, image);
        this.createPost.create(newPost);
        
        
        return new PostCreateResponseDto(
                newPost.getId(),
                newPost.getTitle(),
                newPost.getText(),
                newPost.getImage(),
                newPost.getStatus()
        );
    }
}

