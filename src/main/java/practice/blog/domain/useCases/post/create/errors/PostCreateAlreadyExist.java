package practice.blog.domain.useCases.post.create.errors;

import practice.blog.domain.common.DomainException;

public class PostCreateAlreadyExist extends DomainException {
    public static final String MESSAGE = "Post already exist";
    public PostCreateAlreadyExist() {
        super(PostCreateAlreadyExist.MESSAGE);
    }
}
