package practice.blog.domain.useCases.post.create.dto;

public class PostCreateRequestDto {
    private final String userId;
    private final String title;
    private final String text;
    private final String image;
    
    public PostCreateRequestDto(String userId, String title, String text, String image) {
        this.userId = userId;
        this.title = title;
        this.text = text;
        this.image = image;
    }
    
    public String getUserId() {
        return userId;
    }
    
    public String getTitle() {
        return title;
    }
    
    public String getText() {
        return text;
    }
    
    public String getImage() {
        return image;
    }
}
