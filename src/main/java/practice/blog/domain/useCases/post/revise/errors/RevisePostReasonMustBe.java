package practice.blog.domain.useCases.post.revise.errors;

import practice.blog.domain.common.DomainException;

public class RevisePostReasonMustBe extends DomainException {
    public static final String MESSAGE = "If post not valid, reason must be given";
    
    public RevisePostReasonMustBe() {
        super(RevisePostReasonMustBe.MESSAGE);
    }
}

