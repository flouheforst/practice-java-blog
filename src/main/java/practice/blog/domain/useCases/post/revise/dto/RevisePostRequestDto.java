package practice.blog.domain.useCases.post.revise.dto;


import java.util.Optional;

public class RevisePostRequestDto {
    private final String adminId;
    
    private final String postId;
    
    private final boolean isBlock;
    
    private Optional<String> reason;
    
    public RevisePostRequestDto(String adminId, String postId, boolean isBlock) {
        this.adminId = adminId;
        this.postId = postId;
        this.isBlock = isBlock;
        this.reason = Optional.empty();
    }
    
    public RevisePostRequestDto(String adminId, String postId, boolean isBlock, String reason) {
        this.adminId = adminId;
        this.postId = postId;
        this.isBlock = isBlock;
        this.reason = Optional.of(reason);
    }
    
    public String getAdminId() {
        return adminId;
    }
    
    public String getPostId() {
        return postId;
    }
    
    public boolean getIsBlock() {
        return isBlock;
    }
    
    public Optional<String> getReason() {
        return reason;
    }
}
