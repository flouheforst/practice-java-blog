package practice.blog.domain.useCases.post.revise.errors;

import practice.blog.domain.common.DomainException;

public class RevisePostIsNotNew extends DomainException {
    public static final String MESSAGE = "Post can't be revised because isn't new";
    
    public RevisePostIsNotNew() {
        super(RevisePostIsNotNew.MESSAGE);
    }
}
