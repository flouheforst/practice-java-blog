package practice.blog.domain.useCases.post.create.dto;

import practice.blog.domain.entities.common.PostStatus;

public class PostCreateResponseDto {
    private final String id;
    private final String title;
    private final String text;
    private final String image;
    private final PostStatus status;
    
    public PostCreateResponseDto(String id, String title, String text, String image, PostStatus status) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.image = image;
        this.status = status;
    }
    
    public String getId() {
        return id;
    }
    
    public String getTitle() {
        return title;
    }
    
    public String getText() {
        return text;
    }
    
    public String getImage() {
        return image;
    }
    
    public PostStatus getStatus() {
        return status;
    }
}

