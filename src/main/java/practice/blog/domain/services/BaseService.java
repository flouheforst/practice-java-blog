package practice.blog.domain.services;

import java.util.Optional;

public abstract class BaseService<T> {
    public <V> Optional<T> getByField(String field, V value) {
        return Optional.empty();
    }
    
    public <V> T getByFieldOrFail(String field, V value) {
        final Optional<T> entity = this.getByField(field, value);
        
        if (entity.isEmpty()) {
            throw new RuntimeException("Entity not found");
        }
        
        return (T) entity;
    }
    
    //    public update() {}
    //    public delete() {}
}

