package practice.blog.domain.persistence;

import practice.blog.domain.entities.UserEntity;

public interface CreateUser {
    public UserEntity create(UserEntity user);
}
