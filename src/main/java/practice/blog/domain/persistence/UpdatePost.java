package practice.blog.domain.persistence;


import practice.blog.domain.entities.PostEntity;

public interface UpdatePost {
    PostEntity update(PostEntity post) ;
}

