package practice.blog.domain.persistence;

import practice.blog.domain.entities.PostEntity;

public interface CreatePost {
    PostEntity create(PostEntity post);
}
