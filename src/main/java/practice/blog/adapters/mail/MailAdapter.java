package practice.blog.adapters.mail;

import practice.blog.adapters.mail.dto.MailRequestDto;

public interface MailAdapter {
    public void send(MailRequestDto data);
}
