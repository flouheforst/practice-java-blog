package practice.blog.adapters.mail.dto;

public class MailRequestDto {
    private String email;
    
    private String code;
    
    private MailRequestDto() {
    }
    
    public static Builder builder() {
        return new MailRequestDto().new Builder();
    }
    
    public String getEmail() {
        return email;
    }
    
    public String getCode() {
        return code;
    }
    
    public class Builder {
        private Builder() {
        }
        
        public Builder setEmail(String email) {
            MailRequestDto.this.email = email;
            
            return this;
        }
        
        public Builder setCode(String code) {
            MailRequestDto.this.code = code;
            
            return this;
        }
        
        public MailRequestDto build() {
            return MailRequestDto.this;
        }
    }
}
