package practice.blog.utils;

import java.util.UUID;
import java.util.regex.Pattern;

public class GenerateUuid {
    private static Pattern UUID_REGEX =
            Pattern.compile("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$");
    
    
    public static String UUID() {
        final UUID uuid =  UUID.randomUUID();
        return uuid.toString();
    }
    
    public static Pattern getUuidRegex () {
        return UUID_REGEX;
    }
}
