package practice.blog.utils;

public class GenerateRandomNumber {
    private static final Integer START_SPLIT = 2;
    
    public static String randomNumber(Integer length) {
        
        if (length > 16 || length < 0) {
            throw new RuntimeException("length is too large");
        }
        
        Double response = Math.random();
        
        return response.toString().substring(GenerateRandomNumber.START_SPLIT, length + 2);
    }
}
