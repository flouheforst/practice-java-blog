package practice.blog.domain.useCases.post.revise;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import practice.blog.domain.entities.PostEntity;
import practice.blog.domain.entities.UserEntity;
import practice.blog.domain.entities.valueObjects.ValueObjectException;
import practice.blog.domain.persistence.UpdatePostPersistence;
import practice.blog.domain.services.PostService;
import practice.blog.domain.services.UserService;
import practice.blog.domain.useCases.post.revise.dto.RevisePostRequestDto;
import practice.blog.domain.useCases.post.revise.dto.RevisePostResponseDto;
import practice.blog.domain.useCases.post.revise.errors.RevisePostIsNotNew;
import practice.blog.domain.useCases.post.revise.errors.RevisePostReasonMustBe;
import practice.blog.glodenData.PostEntityData;
import practice.blog.glodenData.UserEntityData;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class PostReviseUseCaseTest {
    private UserService userService;
    private PostService postService;
    RevisePostUseCase revisePostUseCase;
    
    @BeforeEach
    public void setUp() {
        userService = Mockito.mock(UserService.class);
        postService = Mockito.mock(PostService.class);
        
        this.revisePostUseCase = new RevisePostUseCase(
                userService,
                postService,
                new UpdatePostPersistence()
        );
    }
    
    @Test
    void shouldBeReviseCancel() throws ValueObjectException, RevisePostReasonMustBe, RevisePostIsNotNew {
        // given:
        UserEntity user = UserEntityData.generateUser();
        PostEntity post = PostEntityData.generatePost();
        final String reason = "Bad post";
        
        RevisePostRequestDto reviseData = new RevisePostRequestDto(
                user.getId(),
                post.getId(),
                true,
                reason
        );
        
        Mockito
                .when(
                        userService.getByFieldOrFail("id", user.getId())
                )
                .thenReturn(user);
        
        Mockito
                .when(
                        postService.getByFieldOrFail("id", post.getId())
                )
                .thenReturn(post);
        
        
        // when:
        final RevisePostResponseDto reviseResponse = this.revisePostUseCase.execute(reviseData);
        
        // then:
        assertEquals(reviseResponse.getPostId(), reviseData.getPostId());
        assertEquals(reviseResponse.getAdminId(), reviseData.getAdminId());
        assertEquals(reviseResponse.getIsBlock(), reviseData.getIsBlock());
        assertEquals(reviseResponse.getIsBlock(), reviseData.getIsBlock());
    }
    
    
    @Test
    void shouldBeReviseApprove() throws ValueObjectException, RevisePostReasonMustBe, RevisePostIsNotNew {
        // given:
        UserEntity user = UserEntityData.generateUser();
        PostEntity post = PostEntityData.generatePost();
        
        RevisePostRequestDto reviseData = new RevisePostRequestDto(
                user.getId(),
                post.getId(),
                false
        );
        
        Mockito
                .when(
                        userService.getByFieldOrFail("id", user.getId())
                )
                .thenReturn(user);
        
        Mockito
                .when(
                        postService.getByFieldOrFail("id", post.getId())
                )
                .thenReturn(post);
        
        
        // when:
        final RevisePostResponseDto reviseResponse = this.revisePostUseCase.execute(reviseData);
        
        // then:
        assertEquals(reviseResponse.getPostId(), reviseData.getPostId());
        assertEquals(reviseResponse.getAdminId(), reviseData.getAdminId());
        assertEquals(reviseResponse.getIsBlock(), reviseData.getIsBlock());
    }
    
    
    @Test
    void shouldBeCatchPostIsNotNew() throws ValueObjectException, RevisePostReasonMustBe, RevisePostIsNotNew {
        // given:
        UserEntity user = UserEntityData.generateUser();
        PostEntity post = PostEntityData.generatePost();
        
        post.approve();
        
        RevisePostRequestDto reviseData = new RevisePostRequestDto(
                user.getId(),
                post.getId(),
                false
        );
        
        Mockito
                .when(
                        userService.getByFieldOrFail("id", user.getId())
                )
                .thenReturn(user);
        
        Mockito
                .when(
                        postService.getByFieldOrFail("id", post.getId())
                )
                .thenReturn(post);
        
        
        // when:
        RevisePostIsNotNew exception = assertThrows(RevisePostIsNotNew.class, () -> this.revisePostUseCase.execute(reviseData));
        
        // then:
        assertEquals(RevisePostIsNotNew.MESSAGE, exception.getMessage());
    }
    
    @Test
    void shouldCatchPostReasonMustBe() throws ValueObjectException, RevisePostReasonMustBe, RevisePostIsNotNew {
        // given:
        UserEntity user = UserEntityData.generateUser();
        PostEntity post = PostEntityData.generatePost();
        
        RevisePostRequestDto reviseData = new RevisePostRequestDto(
                user.getId(),
                post.getId(),
                true
        );
        
        Mockito
                .when(
                        userService.getByFieldOrFail("id", user.getId())
                )
                .thenReturn(user);
        
        Mockito
                .when(
                        postService.getByFieldOrFail("id", post.getId())
                )
                .thenReturn(post);
        
        
        // when:
        RevisePostReasonMustBe exception = assertThrows(RevisePostReasonMustBe.class, () -> this.revisePostUseCase.execute(reviseData));
        
        // then:
        assertEquals(RevisePostReasonMustBe.MESSAGE, exception.getMessage());
    }
    
    @Test
    void shouldCatchReasonToBig() throws ValueObjectException, RevisePostReasonMustBe, RevisePostIsNotNew {
        // given:
        UserEntity user = UserEntityData.generateUser();
        PostEntity post = PostEntityData.generatePost();
        
        final String reason = "Bad post".repeat(500);
        
        RevisePostRequestDto reviseData = new RevisePostRequestDto(
                user.getId(),
                post.getId(),
                true,
                reason
        );
        
        Mockito
                .when(
                        userService.getByFieldOrFail("id", user.getId())
                )
                .thenReturn(user);
        
        Mockito
                .when(
                        postService.getByFieldOrFail("id", post.getId())
                )
                .thenReturn(post);
        
        
        // when:
//        Throwable thrown = catchThrowable(() -> this.revisePostUseCase.execute(reviseData));
        
        // then:
//        assertThat(thrown).isInstanceOf(ValueObjectException.class);
    }
}
