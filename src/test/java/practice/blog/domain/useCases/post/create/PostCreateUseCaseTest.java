package practice.blog.domain.useCases.post.create;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import practice.blog.domain.entities.UserEntity;
import practice.blog.domain.entities.common.PostStatus;
import practice.blog.domain.entities.valueObjects.ValueObjectException;
import practice.blog.domain.persistence.CreatePost;
import practice.blog.domain.persistence.CreatePostPersistence;
import practice.blog.domain.services.PostService;
import practice.blog.domain.services.UserService;
import practice.blog.domain.useCases.post.create.dto.PostCreateRequestDto;
import practice.blog.domain.useCases.post.create.dto.PostCreateResponseDto;
import practice.blog.glodenData.UserEntityData;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class PostCreateUseCaseTest {
    private UserService userService;
    private PostService postService;
    private PostCreateUseCase postCreateUseCase;
    private CreatePost createPost;
    
    @BeforeEach
    public void setUp() {
        userService = Mockito.mock(UserService.class);
        postService = Mockito.mock(PostService.class);
        createPost = new CreatePostPersistence();
        
        postCreateUseCase = new PostCreateUseCase(userService, postService, createPost);
    }
    
    @Test
    void shouldBeCreated() throws ValueObjectException {
        // given:
        UserEntity userEntity = UserEntityData.generateUser();
        PostCreateRequestDto postCreate = new PostCreateRequestDto(
                userEntity.getId(),
                "Post service",
                "post text",
                "images"
        );
        Mockito
                .when(
                        userService.getByFieldOrFail("id", postCreate.getUserId())
                )
                .thenReturn(userEntity);
        
        //when:
        final PostCreateResponseDto newPost = this.postCreateUseCase.execute(postCreate);
        
        //then:
        assertNotNull(newPost);
    }
    
    @Test
    void shouldBeWithId() throws ValueObjectException {
        // given:
        UserEntity userEntity = UserEntityData.generateUser();
        
        PostCreateRequestDto postCreate = new PostCreateRequestDto(
                userEntity.getId(),
                "Post service",
                "post text",
                "images"
        );
        
        Mockito
                .when(
                        userService.getByFieldOrFail("id", postCreate.getUserId())
                )
                .thenReturn(userEntity);
        
        //when:
        final PostCreateResponseDto newPost = this.postCreateUseCase.execute(postCreate);
        
        //then:
        assertNotNull(newPost.getId());
    }
    
    
    @Test
    void shouldBeWithStatusNew() throws ValueObjectException {
        // given:
        UserEntity userEntity = UserEntityData.generateUser();
        
        PostCreateRequestDto postCreate = new PostCreateRequestDto(
                userEntity.getId(),
                "Post service",
                "post text",
                "images"
        );
        
        Mockito
                .when(
                        userService.getByFieldOrFail("id", postCreate.getUserId())
                )
                .thenReturn(userEntity);
        
        //when:
        final PostCreateResponseDto newPost = this.postCreateUseCase.execute(postCreate);
        
        //then:
        assertEquals(newPost.getStatus(), PostStatus.NEW);
    }
    
    @Test
    void shouldBeCreatedWithTheSameFields() throws ValueObjectException {
        // given:
        UserEntity userEntity = UserEntityData.generateUser();
        PostCreateRequestDto postCreate = new PostCreateRequestDto(
                userEntity.getId(),
                "Post service",
                "post text",
                "images"
        );
        
        Mockito
                .when(
                        userService.getByFieldOrFail("id", postCreate.getUserId())
                )
                .thenReturn(userEntity);
        
        //when:
        final PostCreateResponseDto newPost = this.postCreateUseCase.execute(postCreate);
        
        //then:
        assertEquals(postCreate.getTitle(), newPost.getTitle());
        assertEquals(postCreate.getText(), newPost.getText());
        assertEquals(postCreate.getImage(), newPost.getImage());
    }
    
}

