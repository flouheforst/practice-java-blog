package practice.blog.domain.useCases.user.registration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import practice.blog.adapter.mail.MockMailAdapter;
import practice.blog.adapters.mail.MailAdapter;
import practice.blog.domain.entities.UserEntity;
import practice.blog.domain.entities.common.UserRole;
import practice.blog.domain.entities.valueObjects.ValueObjectException;
import practice.blog.domain.persistence.CreateUser;
import practice.blog.domain.persistence.CreateUserPersistence;
import practice.blog.domain.services.UserService;
import practice.blog.domain.useCases.user.create.UserCreateUseCase;
import practice.blog.domain.useCases.user.registration.dto.UserRegistrationRequestDto;
import practice.blog.domain.useCases.user.registration.dto.UserRegistrationResponseDto;
import practice.blog.glodenData.UserEntityData;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserRegistrationUseCaseTest {
    
    private UserRegistrationUseCase userRegisterUseCase;
    
    @BeforeEach
    public void setUp() {
        
        UserService userService = Mockito.mock(UserService.class);
        CreateUser createUser = new CreateUserPersistence();
        
        UserCreateUseCase userCreateUseCase = new UserCreateUseCase(userService, createUser);
        MailAdapter mailAdapter = new MockMailAdapter();
        
        userRegisterUseCase = new UserRegistrationUseCase(userCreateUseCase, mailAdapter);
    }
    
    @Test
    void shouldUserRegistered() throws ValueObjectException {
        // given:
        UserEntity userEntity = UserEntityData.generateUser();
        
        UserRegistrationRequestDto userRegistrationRequestDto = new UserRegistrationRequestDto(
                userEntity.getEmail(),
                userEntity.getImage(),
                userEntity.getAge()
        );
        
        // when:
        UserRegistrationResponseDto user = this.userRegisterUseCase.execute(userRegistrationRequestDto);
        
        //then:
        assertEquals(userRegistrationRequestDto.getEmail(), user.getEmail());
        assertEquals(userRegistrationRequestDto.getAge(), user.getAge());
        assertEquals(UserRole.CUSTOMER, user.getRole());
        assertEquals(userRegistrationRequestDto.getImage(), user.getImage());
    }
}
