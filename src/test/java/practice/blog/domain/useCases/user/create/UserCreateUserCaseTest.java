package practice.blog.domain.useCases.user.create;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import practice.blog.domain.entities.UserEntity;
import practice.blog.domain.entities.common.UserRole;
import practice.blog.domain.entities.valueObjects.ValueObjectException;
import practice.blog.domain.persistence.CreateUser;
import practice.blog.domain.persistence.CreateUserPersistence;
import practice.blog.domain.services.UserService;
import practice.blog.domain.useCases.user.create.dto.UserCreateRequestDto;
import practice.blog.domain.useCases.user.create.dto.UserCreateResponseDto;
import practice.blog.domain.useCases.user.create.errors.UserCreateAlreadyExist;
import practice.blog.glodenData.UserEntityData;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UserCreateUserCaseTest {
    
    private UserService userService;
    private CreateUser createUser;
    private UserCreateUseCase userCreateUseCase;
    
    @BeforeEach()
    public void setUp() {
        userService = Mockito.mock(UserService.class);
        createUser = new CreateUserPersistence();
        
        userCreateUseCase = new UserCreateUseCase(userService, createUser);
    }
    
    @Test
    void shouldCreateUserCustomer() throws ValueObjectException {
        // given:
        UserEntity userEntity = UserEntityData.generateUser();
        
        UserCreateRequestDto createRequestDto = new UserCreateRequestDto(
                userEntity.getEmail(),
                userEntity.getImage(),
                userEntity.getAge(),
                UserRole.CUSTOMER
        );
        
        // when:
        UserCreateResponseDto responseDto = userCreateUseCase.execute(createRequestDto);
        
        // then
        assertEquals(createRequestDto.getEmail(), responseDto.getEmail());
        assertEquals(createRequestDto.getAge(), responseDto.getAge());
        assertEquals(createRequestDto.getRole(), responseDto.getRole());
        assertEquals(createRequestDto.getImage(), responseDto.getImage());
    }
    
    @Test
    void shouldCreateUserAdmin() throws ValueObjectException {
        // given:
        UserEntity userEntity = UserEntityData.generateUser();
        
        UserCreateRequestDto createRequestDto = new UserCreateRequestDto(
                userEntity.getEmail(),
                userEntity.getImage(),
                userEntity.getAge(),
                UserRole.ADMIN
        );
        
        // when:
        UserCreateResponseDto responseDto = userCreateUseCase.execute(createRequestDto);
        
        // then:
        assertEquals(createRequestDto.getEmail(), responseDto.getEmail());
        assertEquals(createRequestDto.getAge(), responseDto.getAge());
        assertEquals(createRequestDto.getRole(), responseDto.getRole());
        assertEquals(createRequestDto.getImage(), responseDto.getImage());
    }
    
    @Test
    void shouldCatchErrorUserAlreadyExist() throws ValueObjectException {
        // given:
        UserEntity userEntity = UserEntityData.generateUser();
        
        UserCreateRequestDto createRequestDto = new UserCreateRequestDto(
                userEntity.getEmail(),
                userEntity.getImage(),
                userEntity.getAge(),
                UserRole.ADMIN
        );
        
        Mockito
                .when(
                        userService.getByField("email", userEntity.getEmail())
                )
                .thenReturn(Optional.of(userEntity));
        
        
        // when:
        UserCreateAlreadyExist exception = assertThrows(UserCreateAlreadyExist.class, () -> userCreateUseCase.execute(createRequestDto));
        
        // then:
        assertEquals(UserCreateAlreadyExist.MESSAGE, exception.getMessage());
    }
}
