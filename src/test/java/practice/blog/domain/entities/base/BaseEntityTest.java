package practice.blog.domain.entities.base;

import org.junit.jupiter.api.Test;
import practice.blog.domain.entities.BaseEntity;
import practice.blog.utils.GenerateUuid;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class BaseEntityTest {
    @Test
    public void shouldCompareTrue() {
        final String uuid = GenerateUuid.UUID();
        
        final BaseEntity baseFirst = new BaseEntity(uuid);
        final BaseEntity baseSecond = new BaseEntity(uuid);
        
        final boolean isEqual = baseFirst.equals(baseSecond);
        assertTrue(isEqual);
    }
    
    @Test
    public void shouldCompareFalse() {
        final String uuidFirst = GenerateUuid.UUID();
        final String uuidSecond = GenerateUuid.UUID();
        
        final BaseEntity baseFirst = new BaseEntity(uuidFirst);
        final BaseEntity baseSecond = new BaseEntity(uuidSecond);
        
        final boolean isEqual = baseFirst.equals(baseSecond);
        assertFalse(isEqual);
    }
    
    
}
