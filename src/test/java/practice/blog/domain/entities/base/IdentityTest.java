package practice.blog.domain.entities.base;

import org.junit.jupiter.api.Test;
import practice.blog.domain.entities.common.Identity;
import practice.blog.utils.GenerateUuid;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class IdentityTest {
    @Test
    public void shouldGenerateUuid() {
        final Identity identity = new Identity();
        final boolean isUUID = GenerateUuid.getUuidRegex().matcher(identity.getValue()).matches();
        
        assertTrue(isUUID);
    }
    
    @Test
    public void shouldSetUuid() {
        final String uuid = GenerateUuid.UUID();
        final Identity identity = new Identity(uuid);
        
        assertEquals(uuid, identity.getValue());
    }
    
}
