package practice.blog.domain.entities;


import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import practice.blog.domain.entities.valueObjects.ValueObjectException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class UserEntityTest {
    
    private Faker faker;
    
    @BeforeEach
    public void setUp() {
        this.faker = Faker.instance();
    }
    
    @Test
    public void createUser() throws ValueObjectException {
        final String email = this.faker.internet().emailAddress();
        final String image = this.faker.internet().image();
        
        UserEntity userEntity = UserEntity.create(email, image);
        
        assertEquals(email, userEntity.getEmail());
        assertEquals(image, userEntity.getImage());
    }
    
    @Test
    public void usersShouldBeEquals() throws ValueObjectException {
        final String email = this.faker.internet().emailAddress();
        final String image = this.faker.internet().image();
        
        UserEntity userEntity1 = UserEntity.create("1", email, image);
        UserEntity userEntity2 = UserEntity.create("1", email, image);
        
        assertEquals(userEntity1, userEntity2);
    }
    
    @Test
    public void usersShouldBeNotEquals() throws ValueObjectException {
        final String email = this.faker.internet().emailAddress();
        final String image = this.faker.internet().image();
        
        UserEntity userEntity1 = UserEntity.create("1", email, image);
        UserEntity userEntity2 = UserEntity.create("2", email, image);
        
        assertNotEquals(userEntity1, userEntity2);
    }
}

