package practice.blog.domain.entities;


import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import practice.blog.domain.entities.common.PostStatus;
import practice.blog.domain.entities.valueObjects.ValueObjectException;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class PostEntityTest {
    private Faker faker;
    
    @BeforeEach
    public void setUp() {
        this.faker = Faker.instance();
    }
    
    @Test
    public void shouldBeCreated() throws ValueObjectException {
        // given:
        final String title = this.faker.lorem().characters(10, 20);
        final String text = this.faker.lorem().paragraph();
        final String image = this.faker.internet().image();
        
        //when:
        PostEntity postEntity = PostEntity.create(title, text, image);
        
        //then:
        assertEquals(title, postEntity.getTitle());
        assertEquals(text, postEntity.getText());
        assertEquals(image, postEntity.getImage());
    }
    
    @Test
    public void shouldHaveStatusNew() throws ValueObjectException {
        // given:
        final String title = this.faker.lorem().characters(10, 20);
        final String text = this.faker.lorem().paragraph();
        final String image = this.faker.internet().image();
        
        //when:
        PostEntity postEntity = PostEntity.create(title, text, image);
        
        //then:
        assertEquals(PostStatus.NEW, postEntity.getStatus());
    }
    
    @Test
    public void createPostWithError() {
        // given:
        final String title = this.faker.lorem().characters(10, 20).repeat(151);
        final String text = this.faker.lorem().paragraph();
        final String image = this.faker.internet().image();
        
        //when:
        //then:
//        assertThrows(ValueObjectException.class, () -> PostEntity.create(title, text, image));
    }
}

