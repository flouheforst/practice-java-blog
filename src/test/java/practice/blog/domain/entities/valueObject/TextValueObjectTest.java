package practice.blog.domain.entities.valueObject;

import org.junit.jupiter.api.Test;
import practice.blog.domain.entities.validator.Length;
import practice.blog.domain.entities.validator.Validator;
import practice.blog.domain.entities.valueObjects.BaseValueObject;
import practice.blog.domain.entities.valueObjects.TextValueObject;
import practice.blog.domain.entities.valueObjects.ValueObjectException;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class TextValueObjectTest {
    @Test
    public void createTextValueObject() throws ValueObjectException {
        final String text = "q".repeat(10);
        
        BaseValueObject textValueObject = TextValueObject.create(text);
        assertEquals(text, textValueObject.getValue());
    }
    
    @Test
    public void createTextShouldCatchError() {
        final String text = "q".repeat(451);
        
        
        ArrayList<Validator> validators = new ArrayList<>();
        validators.add(new Length(2, 120));
        
        
        Validator validatorEmail = Validator.build(validators);
//        assertThrows(ValueObjectException.class, () -> TextValueObject.create(text, validatorEmail));
    }
    
}

