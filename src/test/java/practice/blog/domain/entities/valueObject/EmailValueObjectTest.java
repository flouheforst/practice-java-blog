package practice.blog.domain.entities.valueObject;


import org.junit.jupiter.api.Test;
import practice.blog.domain.entities.validator.Length;
import practice.blog.domain.entities.validator.Validator;
import practice.blog.domain.entities.valueObjects.BaseValueObject;
import practice.blog.domain.entities.valueObjects.EmailValueObject;
import practice.blog.domain.entities.valueObjects.ValueObjectException;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class EmailValueObjectTest {
    @Test
    public void createEmail() throws ValueObjectException {
        final String email = "username@domain.com";
        
        ArrayList<Validator> validators = new ArrayList<>();
        validators.add(new Length(2, 120));
        
        Validator<String> validatorEmail = Validator.build(validators);
        
        BaseValueObject<String> emailValueObject = EmailValueObject.create(email, validatorEmail);
        
        emailValueObject.getValue();
        assertEquals(email, emailValueObject.getValue());
    }
    
    @Test
    public void createEmailShouldCatchError() {
        final String email = "u".repeat(50) + "@domain.com";
        
        ArrayList<Validator> validators = new ArrayList<>();
        validators.add(new Length(2, 4));
        validators.add(new Length(3, 7));
        validators.add(new Length(7, 17));
        
        Validator<String> validatorEmail = Validator.build(validators);

//        assertThrows(ValueObjectException.class, () -> EmailValueObject.create(email, validatorEmail));
    }
    
    @Test
    public void shouldEquals() throws ValueObjectException {
        final String email1 = "username@domain.com";
        final String email2 = "username@domain.com";
        
        BaseValueObject<String> emailValueObject1 = EmailValueObject.create(email1);
        BaseValueObject<String> emailValueObject2 = EmailValueObject.create(email2);
        
        final boolean isEqual = emailValueObject1.equals(emailValueObject2);
        assertTrue(isEqual);
    }
    
    @Test
    public void shouldNotEquals() throws ValueObjectException {
        final String email1 = "1@domain.com";
        final String email2 = "2@domain.com";
        
        BaseValueObject<String> emailValueObject1 = EmailValueObject.create(email1);
        BaseValueObject<String> emailValueObject2 = EmailValueObject.create(email2);
        
        final boolean isEqual = emailValueObject1.equals(emailValueObject2);
        assertFalse(isEqual);
    }
}

