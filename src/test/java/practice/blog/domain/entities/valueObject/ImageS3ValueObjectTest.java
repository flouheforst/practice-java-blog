package practice.blog.domain.entities.valueObject;

import org.junit.jupiter.api.Test;
import practice.blog.domain.entities.validator.Length;
import practice.blog.domain.entities.validator.Validator;
import practice.blog.domain.entities.valueObjects.BaseValueObject;
import practice.blog.domain.entities.valueObjects.ImageS3ValueObject;
import practice.blog.domain.entities.valueObjects.ValueObjectException;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;


public class ImageS3ValueObjectTest {
    @Test
    public void createEmail() throws ValueObjectException {
        final String email = "username@domain.com";
        
        BaseValueObject<String> emailValueObject = ImageS3ValueObject.create(email);
        emailValueObject.getValue();
        
        assertEquals(email, emailValueObject.getValue());
    }
    
    @Test
    public void createEmailShouldCatchError() {
        final String email = "q".repeat(1025);
        
        
        ArrayList<Validator> validators = new ArrayList<>();
        validators.add(new Length(2, 120));
        
        Validator validatorEmail = Validator.build(validators);

//        assertThrows(ValueObjectException.class, () ->  ImageS3ValueObject.create(email, validatorEmail));
    }
    
    @Test
    public void shouldEquals() throws ValueObjectException {
        final String email1 = "username@domain.com";
        final String email2 = "username@domain.com";
        
        BaseValueObject<String> emailValueObject1 = ImageS3ValueObject.create(email1);
        BaseValueObject<String> emailValueObject2 = ImageS3ValueObject.create(email2);
        
        final boolean isEqual = emailValueObject1.equals(emailValueObject2);
        assertTrue(isEqual);
    }
    
    @Test
    public void shouldNotEquals() throws ValueObjectException {
        final String email1 = "1@domain.com";
        final String email2 = "2@domain.com";
        
        BaseValueObject<String> emailValueObject1 = ImageS3ValueObject.create(email1);
        BaseValueObject<String> emailValueObject2 = ImageS3ValueObject.create(email2);
        
        final boolean isEqual = emailValueObject1.equals(emailValueObject2);
        assertFalse(isEqual);
    }
    
}

