package practice.blog.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GenerateRandomNumberTest {
    
    @Test
    void shouldBeRightLength() {
        // given:
        final int length = 2;
        
        // when:
        final String number = GenerateRandomNumber.randomNumber(length);
        
        // then:
        assertEquals(length, number.length());
    }
}
