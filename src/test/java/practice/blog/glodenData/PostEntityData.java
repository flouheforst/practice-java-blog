package practice.blog.glodenData;

import com.github.javafaker.Faker;
import practice.blog.domain.entities.PostEntity;
import practice.blog.domain.entities.valueObjects.ValueObjectException;
import practice.blog.utils.GenerateUuid;

import java.util.ArrayList;

public class PostEntityData {
    private static Faker faker = Faker.instance();
    
    public static PostEntity generatePost() throws ValueObjectException {
        final String title = PostEntityData.faker.lorem().characters(10, 20);
        final String text = PostEntityData.faker.lorem().characters(10, 20);
        final String image = PostEntityData.faker.internet().image();
        
        final PostEntity post = PostEntity.create(
                GenerateUuid.UUID(),
                title,
                text,
                image
        );
        
        return post;
    }
    
    public static ArrayList<PostEntity> generateListPosts(int size) throws ValueObjectException {
        ArrayList<PostEntity> posts = new ArrayList<>();
        
        for (int i = 0; i < size; i++) {
            final PostEntity post = PostEntityData.generatePost();
            posts.add(post);
        }
        
        return posts;
    }
}

