package practice.blog.glodenData;

import com.github.javafaker.Faker;
import practice.blog.domain.entities.UserEntity;
import practice.blog.domain.entities.valueObjects.ValueObjectException;
import practice.blog.utils.GenerateUuid;

import java.util.ArrayList;

public class UserEntityData {
    private static Faker faker = Faker.instance();
    
    public static UserEntity generateUser() throws ValueObjectException {
        final String email = UserEntityData.faker.internet().emailAddress();
        final String image = UserEntityData.faker.internet().image();
        final Integer age = UserEntityData.faker.number().numberBetween(0, 4);
        
        final UserEntity user = UserEntity.create(
                GenerateUuid.UUID(),
                email,
                image,
                age
        );
        
        return user;
    }
    
    public static ArrayList<UserEntity> generateListUsers(int size) throws ValueObjectException {
        ArrayList<UserEntity> users = new ArrayList<>();
        
        for (int i = 0; i < size; i++) {
            final UserEntity post = UserEntityData.generateUser();
            users.add(post);
        }
        
        return users;
    }
}
