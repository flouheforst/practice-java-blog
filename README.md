# Test blog user

Пользователь может авторизоваться, написать пост, далее администратор проверяет 
и либо публикует, либо отклоняет. Пользователь может ограничить 
доступ для других пользователей.

## Use cases
1. Регистрация пользователя 
   1. Пользователь оставляет свой email и ник 
   2. После чего ему приходит email с кодом доступа 
   3. Пользователь вводит код доступа и входит в систему 
2. Вход в систему 
   1. Пользователь вводит почту ему отправляется email с кодом 
   2. Пользователь вводит код после чего входит в систему 
3. Публикация поста 
   1. Пользователь публикует пост 
   2. Администратор утверждает этот пост 
   3. Пост попадает в общую ленту 
4. Проблемный пост 
   1. Пользователь публикует пост
   2. Администратору пост не нравится он его отклоняет ( указывает причину почему отклонил )
   3. Пост не попадает в общую ленту 
   4. У пользователя он висит как отклоненный и он может его изменить и отправить на модерцию до 3-х раз 
5. Жалобы на пост от других пользователей не обоснованы 
   1. На пост пожаловалось больше 10 человек
   2. Этот пост попадает к админу на рассмотрение, админ отклонил блок 
   3. Пост остается в ленте 
6. Жалобы на пост от других пользователей обоснованы
   1. На пост пожаловалось больше 10 человек
   2. Этот пост попадает к админу на рассмотрение, админ блокирует пост
   3. Пост уходит из ленты 
7. Если пользователь накапливает больше чем 4-х забаненных поста, пользователя банят 
8. Админ регистрация
   1. Админ ввода логин и пароль
   2. Входит в систему 
9. Вход админа просходит по логину и пароолю 


## Тех требования
- Подключить линтер у кода должен быть единый стиль 
- Настроить логирование 
- У приложения должна генерироваться swagger документация 
- Загрузка файлов в s3 
- Реляционная БД 
- Должны быть написаны тесты 
- Настроить мониторинг с prometheus & grafana для приложения и бд 
- Настроить возможность проведение нагрузочного тестирования 
- Добавить очереди для нагрузки с разных транспортов 
- Настроить ci/cd 


## Сущности 
### Пост
- Заголовок 
- Текст 
- Фото
- Статус 

### Пользователь
- email 
- Фото

### Тег
- Название 

### Категория
- Название

## Приложение scripts

```
mvn spring-boot:run - start application
mvn test - start all tests
```